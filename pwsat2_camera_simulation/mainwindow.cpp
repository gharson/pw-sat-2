#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->Cam1Status->setText("OFF");
    ui->Cam1ShotButton->setEnabled(false);
    ui->Cam1StopButton->setEnabled(false);
    ui->Cam1StartButton->setEnabled(true);
    ui->Cam1ProgressBar->reset();

    cam1 = new Camera(640,480,"cam1output");
    cam1Thread = new QThread();

    connect(cam1,SIGNAL(ready()),this,SLOT(cam1Ready()));
    connect(cam1,SIGNAL(ready()),ui->Cam1ProgressBar,SLOT(reset()));
    connect(cam1,SIGNAL(busy(int)),this,SLOT(cam1Busy()));
    connect(cam1,SIGNAL(busy(int)),ui->Cam1ProgressBar,SLOT(setValue(int)));
    connect(ui->Cam1StartButton,SIGNAL(clicked()),cam1,SLOT(start()));
    connect(ui->Cam1ShotButton,SIGNAL(clicked()),cam1,SLOT(takePicture()));
    connect(ui->Cam1TransferRateSpinBox,SIGNAL(valueChanged(double)),cam1,SLOT(setTransfer(double)));
    connect(ui->Cam1TimeMultiplierDoubleSpinBox,SIGNAL(valueChanged(double)),cam1,SLOT(setTimeMultiplier(double)));

    cam1->moveToThread(cam1Thread);
    cam1Thread->start();
}

MainWindow::~MainWindow()
{
    delete ui;
    delete cam1;
    delete cam1Thread;
}

void MainWindow::cam1Ready() {
    ui->Cam1Status->setText("ON");
    ui->Cam1ShotButton->setEnabled(true);
    ui->Cam1StopButton->setEnabled(true);
    ui->Cam1StartButton->setEnabled(false);
}

void MainWindow::cam1Busy() {
    ui->Cam1Status->setText("BUSY");
    ui->Cam1ShotButton->setEnabled(false);
    ui->Cam1StopButton->setEnabled(false);
    ui->Cam1StartButton->setEnabled(false);
}
