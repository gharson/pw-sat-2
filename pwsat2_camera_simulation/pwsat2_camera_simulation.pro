#-------------------------------------------------
#
# Project created by QtCreator 2014-04-12T19:57:58
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = pwsat2_camera_simulation
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    Modules/Camera/camera.cpp \
    Modules/module.cpp \
    Libraries/FileExporter/file_exporter.cpp \
    Libraries/FileImporter/file_importer.cpp

HEADERS  += mainwindow.h \
    Modules/Camera/camera.h \
    Modules/module.h \
    Libraries/FileExporter/file_exporter.h \
    Libraries/FileImporter/file_importer.h

FORMS    += mainwindow.ui
