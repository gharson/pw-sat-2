#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QThread>
#include "Modules/Camera/camera.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    Ui::MainWindow *ui;
    Camera* cam1;
    QThread* cam1Thread;
public slots:
    void cam1Ready();
    void cam1Busy();
};

#endif // MAINWINDOW_H
