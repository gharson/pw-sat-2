#ifndef MEMORY_H
#define MEMORY_H

#include "../module.h"

/**
 * @brief Basic memory module class
 * @author Paweł Szewczyk
 */
class Memory : public Module
{
    Q_OBJECT
    QByteArray buffer;
public:
    Memory();

public slots:
    exportToFile(QString path_file, QByteArray data, int speed);
    importFromFile(QString path_file, QByteArray data, int speed);
signals:

};

#endif // MEMORY_H
