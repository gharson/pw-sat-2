#ifndef CAMERA_H
#define CAMERA_H

#include <QString>

#include "../module.h"
#include "../../Libraries/FileExporter/file_exporter.h"
#include "../../Libraries/FileImporter/file_importer.h"

/**
 * @brief Camera simulation class.
 * @author Paweł Szewczyk
 **/
class Camera : public Module
{
    Q_OBJECT
    QByteArray buffer;
    int size;
    FileExporter exporter;
    FileImporter importer;

public:
    Camera(int w=800, int h=600, QString out="default", double transfer=0);
    double transferRate;
    double timeMultiplier;
    QString outputPath;

public slots:
    void takePicture();
    void start();
    void setTransfer(double);
    void setTimeMultiplier(double);
signals:
    void busy(int);
    void ready();
    void stopped();
};

#endif // CAMERA_H
