#include "camera.h"

Camera::Camera(int w, int h, QString out, double transfer)
{
    size = w*h;
    transferRate = transfer;
    outputPath = out;
    timeMultiplier = 1;
}

void Camera::takePicture() {
    exporter.exportToFile(outputPath,buffer,timeMultiplier*transferRate);
}

void Camera::start() {
    connect(&exporter,SIGNAL(exported()),this,SIGNAL(ready()));
    connect(&exporter,SIGNAL(progressChanged(int)),this,SIGNAL(busy(int)));
    buffer.resize(size);
    emit ready();
}

void Camera::setTransfer(double t) {
    transferRate = t;
}

void Camera::setTimeMultiplier(double t) {
    timeMultiplier = t;
}
