#ifndef MODULE_H
#define MODULE_H

#include <QObject>

class Module : public QObject
{
    Q_OBJECT
public:
    explicit Module(QObject *parent = 0);

signals:
public slots:
    virtual void start() = 0;
};

#endif // MODULE_H
