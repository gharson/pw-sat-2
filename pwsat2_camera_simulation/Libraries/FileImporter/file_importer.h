#ifndef FILE_IMPORTER_H
#define FILE_IMPORTER_H

#include <algorithm>
#include <QObject>
#include <QFile>
#include <QByteArray>
#include <QThread>
#include <QTime>

/*
 * Importowanie z pliku.
 * Author: Piotr Kuligowski / Contact: xsaperx@gmail.com
 * Last update: 30-03-14 by Pawel Szewczyk / Contact: pfpawelec@gmail.com
 */
class FileImporter : public QObject
{
    Q_OBJECT
public:
    explicit FileImporter(QObject *parent = 0);

    QByteArray importFromFile(QString path_file, int speed);

public slots:
    void onImportFromFile(QString path_file, int speed = 0);
signals:
    void imported(QByteArray);
    void progressChanged(int);

private:
    QByteArray buffer;

};

#endif // FILE_IMPORTER_H
