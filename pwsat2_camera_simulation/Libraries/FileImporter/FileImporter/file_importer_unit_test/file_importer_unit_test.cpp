#include "file_importer_unit_test.h"

FileImporterUnitTest::FileImporterUnitTest(QObject *parent) : QObject(parent){
}

void FileImporterUnitTest::importFromFileTest(){
    QByteArray expectedContent;

    expectedContent = "dfsdgfsadhszdfjgsdgjsfgtjsrtksrtkarytjadvfbxcvhaererhrdefnzdfvbz ndfshjzkmzjkzfgmn zgvb";
    QCOMPARE(fileImporter.importFromFile("test_files/kilkadziesiat_bajtow.txt"), expectedContent);

    expectedContent = "d";
    QCOMPARE(fileImporter.importFromFile("test_files/jeden_bajt.txt"), expectedContent);

    expectedContent = "";
    QCOMPARE(fileImporter.importFromFile("test_files/pusty_plik.txt"), expectedContent);

    expectedContent = "";
    QCOMPARE(fileImporter.importFromFile("test_files/nieistniejacy_plik.txt"), expectedContent);
}

//Makro srodowiska testowego
QTEST_MAIN(FileImporterUnitTest)
