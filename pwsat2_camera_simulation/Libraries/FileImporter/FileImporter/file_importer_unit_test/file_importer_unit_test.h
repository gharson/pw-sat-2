#ifndef FILE_IMPORTER_UNIT_TEST_H
#define FILE_IMPORTER_UNIT_TEST_H

#include <QObject>
#include <QByteArray>
#include <QTest>

#include "../file_importer.h"

class FileImporterUnitTest : public QObject
{
    Q_OBJECT
public:
    explicit FileImporterUnitTest(QObject *parent = 0);

private:
    FileImporter fileImporter;

private slots:
    void importFromFileTest();
};

#endif // FILE_IMPORTER_UNIT_TEST_H
