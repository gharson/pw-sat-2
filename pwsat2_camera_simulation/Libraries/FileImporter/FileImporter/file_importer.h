#ifndef FILE_IMPORTER_H
#define FILE_IMPORTER_H

#include <QObject>
#include <QFile>
#include <QByteArray>

/*
 * Wczytywanie pliku.
 * Author: Piotr Kuligowski / Last update: 2014-02-23 / Contact: xsaperx@gmail.com
 */
class FileImporter : public QObject
{
    Q_OBJECT
public:
    explicit FileImporter(QObject *parent = 0);

    QByteArray importFromFile(QString path_file);

};

#endif // FILE_IMPORTER_H
