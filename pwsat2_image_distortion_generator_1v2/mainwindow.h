/*
 * Program uszkadzajacy pliki.
 * Author: Piotr Kuligowski / Last update: 2014-02-23 / Contact: xsaperx@gmail.com / PiotrKuligowski.pl
 */

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QFileDialog>
#include <QMessageBox>
#include "Libraries/DataBuffer/data_buffer.h"
#include "Libraries/FileImporter/file_importer.h"
#include "Libraries/FileExporter/file_exporter.h"

#include "Libraries/DistortionGenerator/RandomGenerator/random_generator.h"
#include "Libraries/DistortionGenerator/RandomByteDistortion/random_byte_distortion.h"

#include <QDebug>

#include <QThread>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_ImportImageButton_clicked();
    void on_ExportImageButton_clicked();
    void on_GenerateRandomByteDistortionButton_clicked();
    void on_ImportImage(QByteArray);
    void disableFileOperations();
    void enableFileOperations();
    void resetThreads();

signals:
    void exportImage(QString,QByteArray,int);
    void importImage(QString,int);

private:
    QString fileName;

    Ui::MainWindow *ui;

    DataBuffer imageBuffer;
    QThread exporterThread;
    QThread importerThread;
    FileImporter imageImporter;
    FileExporter imageExporter;    

    QThread distortionThread;
    RandomByteDistortion randomByteDistortion;
};

#endif // MAINWINDOW_H
