#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->distortionProgressBar->reset();
    ui->cancelButton->setEnabled(true);
    connect(ui->cancelButton, SIGNAL(clicked()), this, SLOT(resetThreads()));

    connect(this, SIGNAL(exportImage(QString,QByteArray,int)), &imageExporter, SLOT(onExportToFile(QString,QByteArray,int)));
    connect(&imageExporter, SIGNAL(progressChanged(int)), ui->distortionProgressBar, SLOT(setValue(int)));
    connect(&imageExporter, SIGNAL(exported()), ui->distortionProgressBar, SLOT(reset()));
    connect(&imageExporter, SIGNAL(exported()), this, SLOT(enableFileOperations()));
    connect(this, SIGNAL(exportImage(QString,QByteArray,int)), this, SLOT(disableFileOperations()));

    imageExporter.moveToThread(&exporterThread);
    exporterThread.start();

    connect(this, SIGNAL(importImage(QString,int)), &imageImporter, SLOT(onImportFromFile(QString,int)));
    connect(&imageImporter, SIGNAL(progressChanged(int)), ui->distortionProgressBar, SLOT(setValue(int)));
    connect(&imageImporter, SIGNAL(imported(QByteArray)), ui->distortionProgressBar, SLOT(reset()));
    connect(&imageImporter, SIGNAL(imported(QByteArray)), this, SLOT(on_ImportImage(QByteArray)));
    connect(&imageImporter, SIGNAL(imported(QByteArray)), this, SLOT(enableFileOperations()));
    connect(this, SIGNAL(importImage(QString,int)), this, SLOT(disableFileOperations()));

    imageImporter.moveToThread(&importerThread);
    importerThread.start();

    //imageImporter.moveToThread(importerThread);
    //randomByteDistortion.moveToThread(distortionThread);
}

MainWindow::~MainWindow(){
    exporterThread.terminate();
    importerThread.terminate();
    //distortionThread.exit();
    delete ui;
}

void MainWindow::on_ImportImageButton_clicked(){
    fileName = QFileDialog::getOpenFileName(this, tr("Open Image"), "", tr("Files (*.*)"));
    emit importImage(fileName, ui->speedSpinBox->value());
}

void MainWindow::on_ImportImage(QByteArray data){
    imageBuffer.setData(data);
    ui->ImportImagePath->setText(fileName);

    if(fileName.count())
        ui->ExportImageButton->setEnabled(true);
    else
        ui->ExportImageButton->setEnabled(false);
}

void MainWindow::on_ExportImageButton_clicked(){
    emit exportImage( (fileName+ui->ExportSuffixEdit->text()), imageBuffer.getData(), ui->speedSpinBox->value());
}

void MainWindow::on_GenerateRandomByteDistortionButton_clicked(){
    imageBuffer.setData( randomByteDistortion.generateRandomByteDistortion(imageBuffer.getData(), ui->PercentRandomByteDistortionSpin->value()) );
}

void MainWindow::disableFileOperations() {
    ui->ExportImageButton->setEnabled(false);
    ui->ImportImageButton->setEnabled(false);
    ui->GenerateRandomByteDistortionButton->setEnabled(false);
    ui->cancelButton->setEnabled(true);
}

void MainWindow::enableFileOperations() {
    ui->ExportImageButton->setEnabled(true);
    ui->ImportImageButton->setEnabled(true);
    ui->GenerateRandomByteDistortionButton->setEnabled(true);
    ui->cancelButton->setEnabled(false);
}

void MainWindow::resetThreads() {
    exporterThread.terminate();
    exporterThread.start();
    importerThread.terminate();
    importerThread.start();
    ui->distortionProgressBar->reset();
    ui->cancelButton->setEnabled(false);
    enableFileOperations();
}

