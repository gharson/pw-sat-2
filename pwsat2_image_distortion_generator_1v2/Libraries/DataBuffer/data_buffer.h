#ifndef DATA_BUFFER_H
#define DATA_BUFFER_H

#include <QObject>
#include <QByteArray>

/*
 * Obsluga bufora danych.
 * Author: Piotr Kuligowski / Last update: 2014-02-22 / Contact: xsaperx@gmail.com
 */
class DataBuffer : private QObject
{
    Q_OBJECT

public:
    explicit DataBuffer(QObject *parent = 0);

    void setData(QByteArray data);
    QByteArray getData();

    quint64 numberOfBytes();
    char getByte(quint64 byteIndex);
    void setByte(quint64 byteIndex, char byte);

    void insertBytes(quint64 byteIndex, QByteArray bytes);
    void deleteBytes(quint64 byteIndex, quint64 numberOfBytes);

    void clear();

private:
    QByteArray dataBuffer;

};

#endif // DATA_BUFFER_H
