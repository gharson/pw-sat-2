#include "data_buffer.h"

DataBuffer::DataBuffer(QObject *parent) : QObject(parent){
}

//Dodawanie danych do bufora danych
void DataBuffer::setData(QByteArray data){  
    dataBuffer = data;
}

//Pobieranie danych z bufora danych
QByteArray DataBuffer::getData(){
    return dataBuffer;
}

//Liczba bajtów w buforze danych
quint64 DataBuffer::numberOfBytes(){
    return dataBuffer.count();
}

//Odczytywanie pojedynczego bajtu z okreslonego miejsca w buforze danych
char DataBuffer::getByte(quint64 byteIndex){
    return dataBuffer.at(byteIndex);
}

//Ustawianie pojedynczego bajtu w okreslone miejsce bufora danych
void DataBuffer::setByte(quint64 byteIndex, char byte){
    dataBuffer.remove(byteIndex, 1);
    dataBuffer.insert(byteIndex, byte);
}

//Dodawanie ciagu bajtow w wybrane miejsce bufora danych
void DataBuffer::insertBytes(quint64 byteIndex, QByteArray bytes){
    dataBuffer.insert(byteIndex, bytes);
}

//Usuwanie ciagu bajtow z wybranego miejsca bufora danych
void DataBuffer::deleteBytes(quint64 byteIndex, quint64 numberOfBytes){
    dataBuffer.remove(byteIndex, numberOfBytes);
}

//Czyszczenie bufora danych
void DataBuffer::clear(){
    dataBuffer.clear();
}
