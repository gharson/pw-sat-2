#include "data_buffer_unit_test.h"

DataBufferUnitTest::DataBufferUnitTest(QObject *parent) : QObject(parent){
}

void DataBufferUnitTest::setData_getData_Test(){
    QByteArray expectedArray;

    dataBuffer.setData("abcdefghijklmnopqrstuvwxyz");
    expectedArray = "abcdefghijklmnopqrstuvwxyz";
    QCOMPARE(dataBuffer.getData(), expectedArray);

    dataBuffer.setData("asfa");
    expectedArray = "asfa";
    QCOMPARE(dataBuffer.getData(), expectedArray);
}

void DataBufferUnitTest::setData_numberOfBytes_Test(){
    quint64 expectedNumber;

    dataBuffer.setData("sahaeegafs");
    expectedNumber = 10;
    QCOMPARE(dataBuffer.numberOfBytes(), expectedNumber);

    dataBuffer.setData("");
    expectedNumber = 0;
    QCOMPARE(dataBuffer.numberOfBytes(), expectedNumber);

    dataBuffer.setData("fgryhkghjyfgryhkghjyfgryhkghjyfgryhkghjyfgryhkghjyfgryhkghjyfgryhkghjyfgryhkghjyfgryhkghjyfgryhkghjy");
    expectedNumber = 100;
    QCOMPARE(dataBuffer.numberOfBytes(), expectedNumber);

    dataBuffer.setData("d");
    expectedNumber = 1;
    QCOMPARE(dataBuffer.numberOfBytes(), expectedNumber);
}

void DataBufferUnitTest::setData_getByte_Test(){
    char expectedByte;

    dataBuffer.setData("qwertyuiop");

    expectedByte = 'q';
    QCOMPARE(dataBuffer.getByte(0), expectedByte);

    expectedByte = 'w';
    QCOMPARE(dataBuffer.getByte(1), expectedByte);

    expectedByte = 'e';
    QCOMPARE(dataBuffer.getByte(2), expectedByte);

    expectedByte = 'r';
    QCOMPARE(dataBuffer.getByte(3), expectedByte);

    expectedByte = 'p';
    QCOMPARE(dataBuffer.getByte(9), expectedByte);
}

void DataBufferUnitTest::setData_setByte_getData_Test(){
    QByteArray expectedArray;

    dataBuffer.setData("qwertyuiop");

    dataBuffer.setByte(1, 'X');
    expectedArray = "qXertyuiop";
    QCOMPARE(dataBuffer.getData(), expectedArray);

    dataBuffer.setByte(9, 'Z');
    expectedArray = "qXertyuioZ";
    QCOMPARE(dataBuffer.getData(), expectedArray);

    dataBuffer.setByte(0, 'U');
    expectedArray = "UXertyuioZ";
    QCOMPARE(dataBuffer.getData(), expectedArray);
}

void DataBufferUnitTest::setData_insertBytes_getData_Test(){
    QByteArray expectedArray;
    QByteArray insertArray;

    dataBuffer.setData("qwertyuiop");

    insertArray = "vsf_";
    dataBuffer.insertBytes(0, insertArray);
    expectedArray = "vsf_qwertyuiop";
    QCOMPARE(dataBuffer.getData(), expectedArray);

    insertArray = "drugi_";
    dataBuffer.insertBytes(0, insertArray);
    expectedArray = "drugi_vsf_qwertyuiop";
    QCOMPARE(dataBuffer.getData(), expectedArray);

    insertArray = "_nakoncu";
    dataBuffer.insertBytes(20, insertArray);
    expectedArray = "drugi_vsf_qwertyuiop_nakoncu";
    QCOMPARE(dataBuffer.getData(), expectedArray);

    insertArray = "_wsrodku_";
    dataBuffer.insertBytes(14, insertArray);
    expectedArray = "drugi_vsf_qwer_wsrodku_tyuiop_nakoncu";
    QCOMPARE(dataBuffer.getData(), expectedArray);
}

void DataBufferUnitTest::setData_deleteBytes_Test(){
    QByteArray expectedArray;

    dataBuffer.setData("qwertyuiop");

    dataBuffer.deleteBytes(0, 1);
    expectedArray = "wertyuiop";
    QCOMPARE(dataBuffer.getData(), expectedArray);

    dataBuffer.deleteBytes(8, 1);
    expectedArray = "wertyuio";
    QCOMPARE(dataBuffer.getData(), expectedArray);

    dataBuffer.deleteBytes(1, 1);
    expectedArray = "wrtyuio";
    QCOMPARE(dataBuffer.getData(), expectedArray);
}

void DataBufferUnitTest::setData_clear_Test(){
    quint64 expectedNumber;

    dataBuffer.setData("qwertyuiop");
    expectedNumber = 10;
    QCOMPARE(dataBuffer.numberOfBytes(), expectedNumber);

    dataBuffer.clear();
    expectedNumber = 0;
    QCOMPARE(dataBuffer.numberOfBytes(), expectedNumber);
}

void DataBufferUnitTest::impossibru_Test() {
    quint64 expectedNumber;
    dataBuffer.setData("qwertyuiop");
    expectedNumber = 2;
    QCOMPARE(dataBuffer.numberOfBytes(), expectedNumber);
}

//Makro srodowiska testowego
QTEST_MAIN(DataBufferUnitTest)
