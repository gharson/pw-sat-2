#-------------------------------------------------
#
# Project created by QtCreator 2014-02-16T22:55:53
#
#-------------------------------------------------

QT       += core testlib

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = data_buffer_unit_test
TEMPLATE = app

SOURCES     += ../data_buffer.cpp\
            data_buffer_unit_test.cpp

HEADERS     += ../data_buffer.h\
            data_buffer_unit_test.h
