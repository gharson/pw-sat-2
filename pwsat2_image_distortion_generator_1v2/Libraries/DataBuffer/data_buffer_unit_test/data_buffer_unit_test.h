#ifndef DATA_BUFFER_UNIT_TEST_H
#define DATA_BUFFER_UNIT_TEST_H

#include <QObject>
#include <QTest>
#include "../data_buffer.h"

#include <QByteArray>

/*
 * Test jednostkowy klasy DataBuffer.
 * Author: Piotr Kuligowski / Last update: 2014-02-22 / Contact: xsaperx@gmail.com
 */
class DataBufferUnitTest : public QObject
{
    Q_OBJECT

public:
    explicit DataBufferUnitTest(QObject *parent = 0);

private:
    DataBuffer dataBuffer;

private slots:
    void setData_getData_Test();
    void setData_numberOfBytes_Test();
    void setData_getByte_Test();
    void setData_setByte_getData_Test();
    void setData_insertBytes_getData_Test();
    void setData_deleteBytes_Test();
    void setData_clear_Test();
    void impossibru_Test();
};

#endif // DATA_BUFFER_UNIT_TEST_H
