#ifndef FILE_EXPORTER_H
#define FILE_EXPORTER_H

#include <algorithm>
#include <QObject>
#include <QFile>
#include <QByteArray>
#include <QString>
#include <QTime>
#include <QThread>

/*
 * Exporter danych do pliku.
 * Author: Piotr Kuligowski / Contact: xsaperx@gmail.com
 * Last update: 30-03-14 by Pawel Szewczyk / Contact: pfpawelec@gmail.com
 */
class FileExporter : public QObject
{
    Q_OBJECT
public:
    explicit FileExporter(QObject *parent = 0);

    void exportToFile(QString path_file, QByteArray data, int speed = 0);
public slots:
    void onExportToFile(QString path_file, QByteArray data, int speed = 0);
signals:
    void exported();
    void progressChanged(int);
};

#endif // FILE_EXPORTER_H
