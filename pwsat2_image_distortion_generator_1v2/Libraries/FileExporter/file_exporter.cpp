#include "file_exporter.h"

FileExporter::FileExporter(QObject *parent) : QObject(parent){
}

/**
 * @brief FileExporter::exportToFile
 * Saves data to file with limited speed.
 * @param path_file path to target file
 * @param data data to be written
 * @param speed optional limit of saving speed in kBps. 0 speed or less writes with full speed.
 */
void FileExporter::exportToFile(QString path_file, QByteArray data, int speed) {
    QFile exported_file;

    exported_file.setFileName(path_file);
    exported_file.open(QIODevice::WriteOnly);

    QTime timer;
    timer.start();
    for(int i=0; i<data.length(); i++) {
        exported_file.putChar(data[i]);
        if(i%64 == 0){
            if(speed > 0)QThread::msleep(std::max(0,i/speed-timer.elapsed()));
            emit progressChanged(100*i/data.length());
        }
    }

    exported_file.close();
    emit exported();
}

void FileExporter::onExportToFile(QString path_file, QByteArray data, int speed) {
    exportToFile(path_file, data, speed);
}
