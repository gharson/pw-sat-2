#include "file_exporter_unit_test.h"

FileExporterUnitTest::FileExporterUnitTest(QObject *parent) : QObject(parent){   
}

void FileExporterUnitTest::exportToFileTest(){
    QByteArray exportedData;
    QString path_file = "test_files/test.txt";

    exportedData = "dane dfasfgag sfha dfhsdgfhsdgfhs dfg hsdgf";
    fileExporter.exportToFile(path_file, exportedData);

    QFile file;
    file.setFileName(path_file);
    file.open(QIODevice::ReadOnly);
    QCOMPARE(file.readAll(), exportedData);
    file.close();
}

//Makro srodowiska testowego
QTEST_MAIN(FileExporterUnitTest)
