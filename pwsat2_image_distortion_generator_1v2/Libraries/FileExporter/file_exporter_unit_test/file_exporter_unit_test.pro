#-------------------------------------------------
#
# Project created by QtCreator 2014-02-16T22:55:53
#
#-------------------------------------------------

QT       += core gui testlib

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = file_exporter
TEMPLATE = app

SOURCES     += ../file_exporter.cpp\
            file_exporter_unit_test.cpp

HEADERS     += ../file_exporter.h\
            file_exporter_unit_test.h
