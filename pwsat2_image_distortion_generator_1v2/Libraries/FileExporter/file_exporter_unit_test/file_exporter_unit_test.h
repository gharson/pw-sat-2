#ifndef FILE_EXPORTER_UNIT_TEST_H
#define FILE_EXPORTER_UNIT_TEST_H

#include <QObject>
#include <QByteArray>
#include <QTest>
#include <QString>
#include "../file_exporter.h"

class FileExporterUnitTest : public QObject
{
    Q_OBJECT
public:
    explicit FileExporterUnitTest(QObject *parent = 0);

private:
    FileExporter fileExporter;

private slots:
    void exportToFileTest();
};

#endif // FILE_EXPORTER_UNIT_TEST_H
