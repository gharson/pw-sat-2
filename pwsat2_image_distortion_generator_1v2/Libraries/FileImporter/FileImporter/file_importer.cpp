#include "file_importer.h"

FileImporter::FileImporter(QObject *parent) : QObject(parent){
}

//Wczytywanie pliku i zwracanie w formie tablicy bajtow
QByteArray FileImporter::importFromFile(QString path_file){
    QByteArray fileData;
    fileData.clear();

    QFile imported_file;
    if(imported_file.exists(path_file)){
        imported_file.setFileName(path_file);
        imported_file.open(QIODevice::ReadOnly);
        fileData = imported_file.readAll();
        imported_file.close();
    }

    return fileData;
}
