#-------------------------------------------------
#
# Project created by QtCreator 2014-02-16T22:55:53
#
#-------------------------------------------------

QT       += core testlib

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = file_importer
TEMPLATE = app

SOURCES     += ../file_importer.cpp\
            file_importer_unit_test.cpp

HEADERS     += ../file_importer.h\
            file_importer_unit_test.h
