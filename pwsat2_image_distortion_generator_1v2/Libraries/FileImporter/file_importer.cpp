#include "file_importer.h"

FileImporter::FileImporter(QObject *parent) : QObject(parent){
}

//Wczytywanie pliku i zwracanie w formie tablicy bajtow
/**
 * @brief Imports data from file with given speed
 * @param path_file path to file
 * @param speed speed of reading in kBps (0 or less read with full speed)
 * @return
 */
QByteArray FileImporter::importFromFile(QString path_file, int speed){
    QByteArray fileData;
    fileData.clear();

    QFile imported_file;
    QTime timer;
    if(imported_file.exists(path_file)){
        imported_file.setFileName(path_file);
        imported_file.open(QIODevice::ReadOnly);
        char c;
        int readData = 0;
        timer.start();
            while(imported_file.getChar(&c)) {
                fileData.append(c);
                readData++;
                if(readData%64 == 0){
                    if(speed > 0) QThread::msleep(readData/speed-timer.elapsed());
                    emit progressChanged(100*readData/imported_file.size());
                }
            }
        imported_file.close();
    }

    return fileData;
}

void FileImporter::onImportFromFile(QString path_file, int speed) {
    buffer = importFromFile(path_file, speed);
    emit imported(buffer);
}
