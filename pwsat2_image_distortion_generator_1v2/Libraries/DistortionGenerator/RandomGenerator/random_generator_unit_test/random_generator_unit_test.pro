#-------------------------------------------------
#
# Project created by QtCreator 2014-02-16T22:55:53
#
#-------------------------------------------------

QT       += core testlib

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = random_generator
TEMPLATE = app

SOURCES     += ../random_generator.cpp\
            random_generator_unit_test.cpp

HEADERS     += ../random_generator.h\
            random_generator_unit_test.h
