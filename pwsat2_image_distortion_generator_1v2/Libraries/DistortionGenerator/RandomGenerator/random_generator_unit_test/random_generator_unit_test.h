#ifndef RANDOM_GENERATOR_UNIT_TEST_H
#define RANDOM_GENERATOR_UNIT_TEST_H

#include <QObject>
#include <QTest>
#include "../random_generator.h"

#include <QDebug>

class RandomGeneratorUnitTest : public QObject
{
    Q_OBJECT
public:
    explicit RandomGeneratorUnitTest(QObject *parent = 0);

private:
    RandomGenerator randomGenerator;

private slots:
    void randomQUint64();
};

#endif // RANDOM_GENERATOR_UNIT_TEST_H
