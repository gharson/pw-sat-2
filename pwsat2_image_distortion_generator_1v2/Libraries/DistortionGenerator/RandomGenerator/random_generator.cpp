#include "random_generator.h"

RandomGenerator::RandomGenerator(QObject *parent) : QObject(parent){
}

quint64 RandomGenerator::randomQUint64(quint64 Low, quint64 High){
    quint64 random_quint64;

    random_quint64 = randomQUint16(0, 32767);
    random_quint64 |= ((quint64)randomQUint16(0, 32767)<<16);
    random_quint64 |= ((quint64)randomQUint16(0, 32767)<<32);
    random_quint64 |= ((quint64)randomQUint16(0, 32767)<<48);

    return random_quint64 % ((High + 1) - Low) + Low;
}

quint16 RandomGenerator::randomQUint16(quint64 Low, quint64 High){
    return (quint16)(qrand() % ((High + 1) - Low) + Low);
}
