#ifndef RANDOM_GENERATOR_H
#define RANDOM_GENERATOR_H

#include <QObject>
#include <QTime>

/*
 * Generator liczb pseudolosowych.
 * Author: Piotr Kuligowski / Last update: 2014-02-23 / Contact: xsaperx@gmail.com
 */
class RandomGenerator : public QObject
{
    Q_OBJECT
public:
    explicit RandomGenerator(QObject *parent = 0);

    quint64 randomQUint64(quint64 Low, quint64 High);
    quint16 randomQUint16(quint64 Low, quint64 High);
};

#endif // RANDOM_GENERATOR_H
