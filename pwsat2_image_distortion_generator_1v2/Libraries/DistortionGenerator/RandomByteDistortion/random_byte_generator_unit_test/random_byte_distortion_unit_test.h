#ifndef RANDOM_BYTE_DISTORTION_UNIT_TEST_H
#define RANDOM_BYTE_DISTORTION_UNIT_TEST_H

#include <QObject>
#include <QByteArray>
#include <QTest>
#include "../random_byte_distortion.h"

#include <QDebug>

class RandomByteDistortionUnitTest : public QObject
{
    Q_OBJECT
public:
    explicit RandomByteDistortionUnitTest(QObject *parent = 0);

private:
    RandomByteDistortion randomByteDistortion;

private slots:
    void generateRandomByteDistortionTest();
};

#endif // RANDOM_BYTE_DISTORTION_UNIT_TEST_H
