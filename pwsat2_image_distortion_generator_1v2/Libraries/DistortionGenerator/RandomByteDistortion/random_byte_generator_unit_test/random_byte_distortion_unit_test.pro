#-------------------------------------------------
#
# Project created by QtCreator 2014-02-16T22:55:53
#
#-------------------------------------------------

QT       += core testlib

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = random_byte_distortion
TEMPLATE = app

SOURCES     += ../random_byte_distortion.cpp\
            random_byte_distortion_unit_test.cpp

HEADERS     += ../random_byte_distortion.h\
            random_byte_distortion_unit_test.h
