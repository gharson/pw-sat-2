#include "random_byte_distortion_unit_test.h"

RandomByteDistortionUnitTest::RandomByteDistortionUnitTest(QObject *parent) : QObject(parent){
}

void RandomByteDistortionUnitTest::generateRandomByteDistortionTest(){
    QByteArray wejscie;
    wejscie = "abcdefghij";

    qDebug() << randomByteDistortion.generateRandomByteDistortion(wejscie,-1);
    qDebug() << randomByteDistortion.generateRandomByteDistortion(wejscie,0);
    qDebug() << randomByteDistortion.generateRandomByteDistortion(wejscie,10);
    qDebug() << randomByteDistortion.generateRandomByteDistortion(wejscie,20);
    qDebug() << randomByteDistortion.generateRandomByteDistortion(wejscie,30);
    qDebug() << randomByteDistortion.generateRandomByteDistortion(wejscie,40);
    qDebug() << randomByteDistortion.generateRandomByteDistortion(wejscie,50);
    qDebug() << randomByteDistortion.generateRandomByteDistortion(wejscie,60);
    qDebug() << randomByteDistortion.generateRandomByteDistortion(wejscie,70);
    qDebug() << randomByteDistortion.generateRandomByteDistortion(wejscie,80);
    qDebug() << randomByteDistortion.generateRandomByteDistortion(wejscie,90);
    qDebug() << randomByteDistortion.generateRandomByteDistortion(wejscie,100);
    qDebug() << randomByteDistortion.generateRandomByteDistortion(wejscie,101);
}

//Makro srodowiska testowego
QTEST_MAIN(RandomByteDistortionUnitTest)
