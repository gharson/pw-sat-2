#include "random_byte_distortion.h"

RandomByteDistortion::RandomByteDistortion(QObject *parent) : QObject(parent){
}

QByteArray RandomByteDistortion::generateRandomByteDistortion(QByteArray input_data, double percent_of_distortion){
    quint64 distortion_bytes = 0;
    quint64 index_of_distorted_byte;
    quint64 input_data_count = input_data.count();

    percent_of_distortion = checkPercentOfDistortion(percent_of_distortion);

    if((!percent_of_distortion)or(!input_data.count()))
        return input_data;

    distortion_bytes = distortionBytes(input_data_count, percent_of_distortion);

    if(!distortion_bytes)
        return input_data;

    for(index_of_distorted_byte=0; index_of_distorted_byte<distortion_bytes; index_of_distorted_byte++){
        input_data = replaceChar(input_data, randomCharIndex(input_data_count), randomChar());
    }

    return input_data;
}

double RandomByteDistortion::checkPercentOfDistortion(double percent_of_distortion){
    if(percent_of_distortion>100.0)
        percent_of_distortion = 100.0;

    if(percent_of_distortion<0.0)
        percent_of_distortion = 0.0;

    return percent_of_distortion;
}

quint64 RandomByteDistortion::distortionBytes(quint64 data_count, double percent_of_distortion){
    return round((double)data_count*percent_of_distortion*0.01);
}

QByteArray RandomByteDistortion::replaceChar(QByteArray input_data, quint64 index_of_char, char new_char){
    input_data.remove(index_of_char, 1);
    input_data.insert(index_of_char, new_char);

    return input_data;
}

quint64 RandomByteDistortion::randomCharIndex(quint64 input_data_count){
    return randomGenerator.randomQUint64(0, input_data_count);
}

char RandomByteDistortion::randomChar(){
    return (char)randomGenerator.randomQUint64(0, 255);
}
