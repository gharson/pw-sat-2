#ifndef RANDOM_BYTE_DISTORTION_H
#define RANDOM_BYTE_DISTORTION_H

#include <QObject>
#include <QByteArray>
#include <math.h>
#include "Libraries/DistortionGenerator/RandomGenerator/random_generator.h"
#include <QDebug>

/*
 * Generator losowych uszkodzen bajtow.
 * Author: Piotr Kuligowski / Last update: 2014-02-23 / Contact: xsaperx@gmail.com
 */
class RandomByteDistortion : public QObject
{
    Q_OBJECT
public:
    explicit RandomByteDistortion(QObject *parent = 0);

    QByteArray generateRandomByteDistortion(QByteArray input_data, double percent_of_distortion);

private:
    double checkPercentOfDistortion(double percent_of_distortion);

    quint64 distortionBytes(quint64 data_count, double percent_of_distortion);

    QByteArray replaceChar(QByteArray input_data, quint64 index_of_char, char new_char);

    quint64 randomCharIndex(quint64 input_data_count);
    char randomChar();

    RandomGenerator randomGenerator;
};

#endif // RANDOM_BYTE_DISTORTION_H
