#-------------------------------------------------
#
# Project created by QtCreator 2014-02-21T19:57:54
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = pwsat2_image_distortion_generator
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp\
        Libraries/DataBuffer/data_buffer.cpp\
        Libraries/FileImporter/file_importer.cpp\
        Libraries/FileExporter/file_exporter.cpp\
        Libraries/DistortionGenerator/RandomGenerator/random_generator.cpp\
        Libraries/DistortionGenerator/RandomByteDistortion/random_byte_distortion.cpp

HEADERS  += mainwindow.h\
         Libraries/DataBuffer/data_buffer.h\
         Libraries/FileImporter/file_importer.h\
         Libraries/FileExporter/file_exporter.h\
         Libraries/DistortionGenerator/RandomGenerator/random_generator.h\
         Libraries/DistortionGenerator/RandomByteDistortion/random_byte_distortion.h

FORMS    += mainwindow.ui
